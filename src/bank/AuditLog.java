package bank;

import java.util.ArrayList;
import java.util.List;

public class AuditLog {
	private List<String> log = new ArrayList<String>();

	static private AuditLog auditLog = new AuditLog();

	// prevent instantiation from other classes
	private AuditLog(){};
	
	static public AuditLog getInstance() {
		return auditLog;
	}
	
	public void addEntry(String entry) {
		log.add(entry);
	}
	
	public String getLog() {
		String logImage = "";
		for (String logEntry : log) {
			if (logImage.length() != 0) {logImage += "\n";}
			logImage += logEntry;
		}
		return logImage;
	}
}
