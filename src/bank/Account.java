package bank;

import java.util.HashMap;
import java.util.Map;

public class Account {
	private static Map<String, Float> accountBalance = new HashMap<String, Float>();
	private static Map<String, String> accountRules = new HashMap<String, String>();
	//private String name;
	
	private AuditLog auditLog = AuditLog.getInstance();
	
	public static final String CONSUMER_RULES = "Consumer";
	public static final String BUSINESS_RULES = "Business";
	public static final String GOVERNMENT_RULES = "Government";

	private AccountRules Rules;
	
	public AuditLog getAuditLog ()
	{
		return auditLog;
	}
	
	public Account (String name, String rules) throws Exception
	{
		//this.name = name;
		
		if (accountRules.containsKey(name)) {
			throw new Exception("Account " + name
					+ " already exists");
		}
		if (CONSUMER_RULES.equals(rules)
				|| BUSINESS_RULES.equals(rules)
				|| GOVERNMENT_RULES.equals(rules)) {
			Rules = AccountRules.getRules(rules);
			
			//System.out.println(name + "->" + rules);
			accountBalance.put(name, 0.0F);
			accountRules.put(name, rules);
			//System.out.println(accountBalance.get(name) + "->" + accountRules.get(name));
			
			
			auditLog.addEntry("Adding account for " + name);
		
		} else {
			throw new Exception("Unknown rule type " + rules);
		}
	}
	
	
	
	public void deposit (String name, float amount) throws Exception
	{
		Rules = AccountRules.getRules(accountRules.get(name));
		//String name = request.name;
		String rule = accountRules.get(name);
		//System.out.println("rule check :  " + accountRules.get(name));
		if (rule == null) {
			throw new Exception("Unknown account " + name);
		}
		float balance = accountBalance.get(name);
		
		balance= Rules.addMoney(balance, amount);
		accountBalance.put(name, balance);
		
		//System.out.println(accountBalance.get(name));
	}
	
	
	
	public void withdraw(String name, float amount) throws Exception 
	{
		Rules = AccountRules.getRules(accountRules.get(name));
		//String name = request.name;
		String rule = accountRules.get(name);
		if (rule == null) {
			throw new Exception("Unknown account " + name);
		}
		float balance = accountBalance.get(name);

		//System.out.println("REMOVE$$$ -> " + balance + " " + amount);
		
		balance = Rules.removeMoney(balance, amount);
		
		//System.out.println("after remove " + balance + " " + amount);
		
		accountBalance.put(name, balance);
	}
	
	
	
	
	public String getStatement(String name)
	{
		//System.out.println ("in account, get statement " + name);
		return "Customer: " + name + ", balance: " + accountBalance.get(name)
				+ " rule: " + accountRules.get(name);
	}
	
	
}