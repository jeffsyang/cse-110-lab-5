package bank;

import java.util.HashMap;
import java.util.Map;

public class Bank {

	//private AuditLog auditLog = AuditLog.getInstance();

	//public static final String CONSUMER_RULES = "Consumer";
	//public static final String BUSINESS_RULES = "Business";
	
	private Account bankAccount;



	public Object dispatch(Request request) throws Exception {
		Object returnVal = null;
		
		if (request.operation.equals("addAccount")) {
			returnVal = bankAccount = new Account (request.name, request.param1);
		} 
		
		else if (request.operation.equals("withdraw")) {
			bankAccount.withdraw(request.name, Float.parseFloat(request.param1));
		} 
		
		else if (request.operation.equals("deposit")) {
			bankAccount.deposit(request.name, Float.parseFloat(request.param1));
		} 
		
		else if (request.operation.equals("getStatement")) {
			System.out.println (bankAccount.getStatement(request.name));
		} 
		
		else if (request.operation.equals("getLog")) {
			return bankAccount.getAuditLog().getLog();
		} 
		
		else {
			throw (new Exception("Unknown request " + request.operation));
		}
		return returnVal;
	}


/*
	private Object getStatement(Request request) {
		String name = request.name;
		float balance = accountBalance.get(name);
		return "Customer: " + name + ", balance: " + balance;
	}



	private void deposit(String name, float amount) throws Exception {
		//String name = request.name;
		String rule = accountRules.get(name);
		if (rule == null) {
			throw new Exception("Unknown account " + name);
		}
		float balance = accountBalance.get(name);
		//float amount = Float.parseFloat(request.param1);
		if (rule.equals(CONSUMER_RULES)) {
			auditLog.addEntry("Account " + name + ": Adding " + amount);
			balance += amount;
		} else if (rule.equals(BUSINESS_RULES)) {
			float fee = amount * 0.01F;
			float deposit = amount - fee;
			auditLog.addEntry("Account " + name + ": Adding " + (amount - fee) + " after fee " + fee);
			balance += deposit;
		}
		accountBalance.put(name, balance);
	}

	
	
	private void withdraw(String name, float amount) throws Exception {
		//String name = request.name;
		String rule = accountRules.get(name);
		if (rule == null) {
			throw new Exception("Unknown account " + name);
		}
		float balance = accountBalance.get(name);
		//float amount = Float.parseFloat(request.param1);
		if (rule.equals(CONSUMER_RULES)) {
			if (balance - amount >= 0) {
				auditLog.addEntry("Account " + name + ": Removing " + amount);
				balance -= amount;
			} else {
				auditLog.addEntry("Rejected withdrawal of " + amount);
				throw new Exception("Not allowed to overdraw");
			}
		} else if (rule.equals(BUSINESS_RULES)) {
			if (balance - amount >= 100) {
				auditLog.addEntry("Account " + name + ": Removing " + amount);
				balance -= amount;
			} else {
				auditLog.addEntry("Rejected withdrawal of " + amount);
				throw new Exception("Not allowed to overdraw");
			}
		}
		accountBalance.put(name, balance);
	}

	
	
	private Object addAccount(String name, String accountType) throws Exception {
		if (accountRules.containsKey(accountType)) {
			throw new Exception("Account " + name
					+ " already exists");
		}
		if (CONSUMER_RULES.equals(accountType)
				|| BUSINESS_RULES.equals(accountType)) {
			accountBalance.put(name, 0.0F);
			accountRules.put(name, accountType);
			auditLog.addEntry("Adding account for " + name);
			return null;
		} else {
			throw new Exception("Unknown rule type " + accountType);
		}
	}
	*/

	public static void main(String[] args) {
		Bank bank = new Bank();
		try
		{
			bank.dispatch(new Request("addAccount", "Barry", Account.CONSUMER_RULES)); 
			bank.dispatch(new Request("addAccount", "Fedex", Account.BUSINESS_RULES)); 
			bank.dispatch(new Request("addAccount", "UCSD", Account.GOVERNMENT_RULES));
			
			bank.dispatch(new Request("deposit", "Barry", "200")); 
			bank.dispatch(new Request("deposit", "Fedex", "500")); 
			bank.dispatch(new Request("deposit", "UCSD", "700")); 
			
			bank.dispatch(new Request("withdraw", "Barry", "50")); 
			//System.out.println(bank.dispatch(new Request("getStatement", "Barry"))); 
			bank.dispatch(new Request("withdraw", "Barry", "149"));
			
			bank.dispatch(new Request("getStatement", "Barry"));
			bank.dispatch(new Request("getStatement", "Fedex"));
			bank.dispatch(new Request("getStatement", "UCSD"));
			
			//bank.dispatch(new Request("deposit", "Fedex", "2000")); 
		}
		catch (Exception e) {
			System.out.println("Exception: " + e);
		}
	}
}
