package bank;

public class GovernmentRules extends AccountRules {
	public GovernmentRules() {
		super();
	}
	
	@Override
	public float addMoney(float balance, float amount) {
		return balance + amount;
	}

	@Override
	public float removeMoney(float balance, float amount) throws Exception {
		if (balance - amount >= 5000) {
			auditLog.addEntry("Account " + name + ": Removing " + amount);
			balance -= amount;
		} else {
			auditLog.addEntry("Rejected withdrawal of " + amount);
			throw new Exception("Not allowed to overdraw");
		}
		return balance;
	}

}
