package bank;

public class ConsumerRules extends AccountRules {

	public ConsumerRules() {
		super();
	}

	@Override
	public float addMoney(float balance, float amount) {
		auditLog.addEntry("Account " + name + ": Adding " + amount);
		balance += amount;
		return balance;
	}

	@Override
	public float removeMoney(float balance, float amount) throws Exception {
		if (balance - amount >= 0) {
			auditLog.addEntry("Account " + name + ": Removing " + amount);
			balance -= amount;
		} else {
			auditLog.addEntry("Rejected withdrawal of " + amount);
			throw new Exception("Not allowed to overdraw");
		}
		return balance;
	}

}
