package bank;

public class BusinessRules extends AccountRules {
	public BusinessRules() {
		super();
	}

	@Override
	public float addMoney(float balance, float amount) {
		float fee = amount * 0.01F;
		float deposit = amount - fee;
		auditLog.addEntry("Account " + name + ": Adding " + (amount - fee) + " after fee " + fee);
		balance += deposit;
		return balance;
	}

	@Override
	public float removeMoney(float balance, float amount) throws Exception {
		if (balance - amount >= 100) {
			auditLog.addEntry("Account " + name + ": Removing " + amount);
			balance -= amount;
		} else {
			auditLog.addEntry("Rejected withdrawal of " + amount);
			throw new Exception("Not allowed to overdraw");
		}
		return balance;
	}

}
