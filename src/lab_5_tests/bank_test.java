package lab_5_tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import bank.Bank;
import bank.Request;

public class bank_test
{
	@Test
	public void test()
	{
		//initial setup
		Bank bank = new Bank();
		Request req1 = new Request("addAccount", "Nghi", "Consumer");
		Request req2 = new Request("deposit", "Nghi", "100");
		try {assertEquals(null, bank.dispatch(req1));}
			catch(Exception e) {System.out.println("Exception: " + e);}
		
		//operation deposit outside_1
		try {bank.dispatch(req2);}
			catch(Exception e) {System.out.println("Exception: " + e);}
		
		//operation addAccount
		try {bank.dispatch(new Request("addAccount", "Nghi", null));}
			catch (Exception e) {System.out.println("Exception: " + e);}
		try {bank.dispatch(new Request("addAccount", "Jeff", null));}
			catch (Exception e) {System.out.println("Exception: " + e);}
		try {assertEquals(null, bank.dispatch(new Request("addAccount", "Jeff", "Business")));}
			catch (Exception e) {System.out.println("Exception: " + e);}
		
		//operation deposit outside_2
		try {bank.dispatch(new Request("deposit", "Jeff", "500"));}
			catch(Exception e) {System.out.println("Exception: " + e);}
		
		//operation withdraw
		try {bank.dispatch(new Request("withdraw", "Mike", null));}
			catch (Exception e) {System.out.println("Exception: " + e);}
		try {bank.dispatch(new Request("withdraw", "Nghi", "150"));}
			catch (Exception e) {System.out.println("Exception: " + e + " (Nghi)");}
		try {bank.dispatch(new Request("withdraw", "Nghi", "50"));}
			catch (Exception e) {System.out.println("Exception: " + e);}
		try {bank.dispatch(new Request("withdraw", "Jeff", "500"));}
			catch (Exception e) {System.out.println("Exception: " + e + " (Jeff)");}
		try {bank.dispatch(new Request("withdraw", "Jeff", "100"));}
			catch (Exception e) {System.out.println("Exception: " + e);}
		
		//operation deposit
		try {bank.dispatch(new Request("deposit", "Mike", null));}
			catch(Exception e) {System.out.println("Exception: " + e);}
		
		//operation getStatement
		try {bank.dispatch(new Request("getStatement", "Nghi", null));}
			catch(Exception e) {System.out.println("Exception: " + e);}
		
		//operation getLog
		try {assertNotNull(bank.dispatch(new Request("getLog", "Mike", null)));}
			catch(Exception e) {System.out.println("Exception: " + e);}
		
		//operation others
		try {assertNotNull(bank.dispatch(new Request("blah", "Mike", null)));}
			catch(Exception e) {System.out.println("Exception: " + e);}
	}

}
